import { createApp } from 'vue'
import './style.scss'
import App from './App.vue'
import { router } from './routes/index'

import about from './components/icons/about.vue'
import aliens from './components/icons/aliens.vue'
import box from './components/icons/box.vue'
import clients from './components/icons/clients.vue'
import home from './components/icons/home.vue'
import hospital from './components/icons/hospital.vue'

import Checkbox from './components/ui-kit/control/Checkbox.vue'
import Button from './components/ui-kit/control/Button.vue'
import { createPinia } from 'pinia'

const app = createApp(App)
const pinia = createPinia()

app.use(pinia)
app.use(router)

// ICONS
app.component('icon-about', about)
app.component('icon-aliens', aliens)
app.component('icon-box', box)
app.component('icon-clients', clients)
app.component('icon-home', home)
app.component('icon-hospital', hospital)
// COMPONENTS
app.component('Checkbox', Checkbox)
app.component('Button', Button)

app.mount('#app')