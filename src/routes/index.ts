import { createRouter, createWebHistory } from "vue-router"
import Home from "../views/Home.vue"
import Clients from "../views/Clients.vue"
import Client from "../views/Client.vue"

const routes = [
  { path: '/', component: Home },
  { path: '/clients', component: Clients },
  { path: '/clients/:id', component: Client },
]

export const router = createRouter({
  history: createWebHistory(),
  routes,
})
