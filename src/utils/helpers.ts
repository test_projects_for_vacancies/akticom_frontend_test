export function formatPhone (phone: string|number) {
  phone = phone.toString()
  const result = ''
    + phone.slice(0, -10)
    + ' ('
    + phone.slice(-10, -7)
    + ') '
    + phone.slice(-7, -4)
    + '-'
    + phone.slice(-4, -2)
    + '-'
    + phone.slice(-2)

  return result
}

export function orderAscByKey (a: any, b: any, key: string) {
  if (a[key] < b[key]) {
    return -1
  }
  if (a[key] > b[key]) {
    return 1
  }
  return 0
}

export function orderDescByKey (a: any, b: any, key: string) {
  if (a[key] < b[key]) {
    return 1
  }
  if (a[key] > b[key]) {
    return -1
  }
  return 0
}

export function unique(arr: any): any {
  return Array.from(new Set(arr))
}