export interface ITableItem {
  id: number,
  [key:string]: string|number,
}

export interface IBaseTableProps {
  headers: NameValueType[],
  data?: ITableItem[],
  isFilterable?: boolean
  isSortable?: boolean
}

export type OrderByType = {
  key: string|null,
  direction: 'asc'|'desc',
}
