export interface NavItemType {
  url: string,
  text: string,
  icon: string,
  target?: '_blank'|'_self'|'_parent'|'_top',
}

export type NameValueType = {
  name: string,
  value: string,
}

export type ShortClientType = {
  id: number,
  fullname: string,
  created_at: string,
  phone: string,
  region: string,
  status: string,
}
