module.exports = {
  '.text-display-1' : {
    fontSize: '2.25rem',
    lineHeight: '2.5rem',

    '@screen xl': {
      fontSize: '3.5rem',
      lineHeight: '4rem',
    },
  },
  '.text-display-2' : {
    fontSize: '2rem',
    lineHeight: '2.5rem',

    '@screen xl': {
      fontSize: '2.75rem',
      lineHeight: '3rem',
    },
  },
  '.text-h1' : {
    fontSize: '1.26rem',
    lineHeight: '1.5rem',

    '@screen md': {
      fontSize: '1.5rem',
      lineHeight: '2rem',
    },
  },
  '.text-h2' : {
    fontSize: '1rem',
    lineHeight: '1.5rem',

    '@screen md': {
      fontSize: '1.25rem',
      lineHeight: '1.75rem',
    },
  },
}
