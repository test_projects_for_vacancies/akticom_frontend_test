module.exports = {
  '.grid-default': {
    display: 'grid',
    gridTemplateColumns: 'repeat(2, minmax(0, 1fr))',
    gap: '1.5rem',

    '@screen md': {
      gap: '2.5rem',
      gridTemplateColumns: 'repeat(8, minmax(0, 1fr))',
    },

    '@screen xl': {
      gridTemplateColumns: 'repeat(12, minmax(0, 1fr))',
    },
  },
}