module.exports = {
  '.centered' : {
    transform: 'translate(-50%, -50%, 0)',
    top: '50%',
    left: '50%',
  },
}
