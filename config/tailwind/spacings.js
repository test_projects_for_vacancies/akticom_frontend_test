module.exports = {
// ------------------------------------- Responsive spacings ------------------------------------->>
// ------------------------------------------- Margin -------------------------------------------->>
// xs
  '.m-xs': {
    margin: '1.25rem',

    '@screen md': {
      margin: '1.5rem',
    },
  },
  '.mt-xs': {
    marginTop: '1.25rem',

    '@screen md': {
      marginTop: '1.5rem',
    },
  },
  '.mr-xs': {
    marginRight: '1.25rem',

    '@screen md': {
      marginRight: '1.5rem',
    },
  },
  '.mb-xs': {
    marginBottom: '1.25rem',

    '@screen md': {
      marginBottom: '1.5rem',
    },
  },
  '.ml-xs': {
    marginLeft: '1.25rem',

    '@screen md': {
      marginLeft: '1.5rem',
    },
  },
  '.my-xs': {
    marginTop: '1.25rem',
    marginBottom: '1.25rem',

    '@screen md': {
      marginTop: '1.5rem',
      marginBottom: '1.5rem',
    },
  },
  // sm
  '.m-sm': {
    margin: '2rem',

    '@screen md': {
      margin: '2.5rem',
    },
  },
  '.mt-sm': {
    marginTop: '2rem',

    '@screen md': {
      marginTop: '2.5rem',
    },
  },
  '.mr-sm': {
    marginRight: '2rem',

    '@screen md': {
      marginRight: '2.5rem',
    },
  },
  '.mb-sm': {
    marginBottom: '2rem',

    '@screen md': {
      marginBottom: '2.5rem',
    },
  },
  '.ml-sm': {
    marginLeft: '2rem',

    '@screen md': {
      marginLeft: '2.5rem',
    },
  },
  // md
  '.m-md': {
    margin: '2.5rem',

    '@screen md': {
      margin: '4rem',
    },
  },
  '.mt-md': {
    marginTop: '2.5rem',

    '@screen md': {
      marginTop: '4rem',
    },
  },
  '.mr-md': {
    marginRight: '2.5rem',

    '@screen md': {
      marginRight: '4rem',
    },
  },
  '.mb-md': {
    marginBottom: '2.5rem',

    '@screen md': {
      marginBottom: '4rem',
    },
  },
  '.ml-md': {
    marginLeft: '2.5rem',

    '@screen md': {
      marginLeft: '4rem',
    },
  },
  // lg
  '.m-lg': {
    margin: '4rem',

    '@screen md': {
      margin: '5rem',
    },
  },
  '.mt-lg': {
    marginTop: '4rem',

    '@screen md': {
      marginTop: '5rem',
    },
  },
  '.mr-lg': {
    marginRight: '4rem',

    '@screen md': {
      marginRight: '5rem',
    },
  },
  '.mb-lg': {
    marginBottom: '4rem',

    '@screen md': {
      marginBottom: '5rem',
    },
  },
  '.ml-lg': {
    marginLeft: '4rem',

    '@screen md': {
      marginLeft: '5rem',
    },
  },
  // xl
  '.m-xl': {
    margin: '5rem',

    '@screen md': {
      margin: '7.5rem',
    },
  },
  '.mt-xl': {
    marginTop: '5rem',

    '@screen md': {
      marginTop: '7.5rem',
    },
  },
  '.mr-xl': {
    marginRight: '5rem',

    '@screen md': {
      marginRight: '7.5rem',
    },
  },
  '.mb-xl': {
    marginBottom: '5rem',

    '@screen md': {
      marginBottom: '7.5rem',
    },
  },
  '.ml-xl': {
    marginLeft: '5rem',

    '@screen md': {
      marginLeft: '7.5rem',
    },
  },
  // xxl
  '.m-xxl': {
    margin: '7.5rem',

    '@screen md': {
      margin: '10rem',
    },
  },
  '.mt-xxl': {
    marginTop: '7.5rem',

    '@screen md': {
      marginTop: '10rem',
    },
  },
  '.mr-xxl': {
    marginRight: '7.5rem',

    '@screen md': {
      marginRight: '10rem',
    },
  },
  '.mb-xxl': {
    marginBottom: '7.5rem',

    '@screen md': {
      marginBottom: '10rem',
    },
  },
  '.ml-xxl': {
    marginLeft: '7.5rem',

    '@screen md': {
      marginLeft: '10rem',
    },
  },

  '.-my-sm': {
    marginTop: '-2rem',
    marginBottom: '-2rem',

    '@screen md': {
      marginTop: '-2.5rem',
      marginBottom: '-2.5rem',
    },
  },
  '.-mt-sm': {
    marginTop: '-2rem',

    '@screen md': {
      marginTop: '-2.5rem',
    },
  },
  '.-mb-xs': {
    marginBottom: '-1.25rem',

    '@screen md': {
      marginBottom: '-1.5rem',
    },
  },
// ------------------------------------------ Padding -------------------------------------------->>
  // xs
  '.p-xs': {
    padding: '1.25rem',

    '@screen md': {
      padding: '1.5rem',
    },
  },
  '.pt-xs': {
    paddingTop: '1.25rem',

    '@screen md': {
      paddingTop: '1.5rem',
    },
  },
  '.pr-xs': {
    paddingRight: '1.25rem',

    '@screen md': {
      paddingRight: '1.5rem',
    },
  },
  '.pb-xs': {
    paddingBottom: '1.25rem',

    '@screen md': {
      paddingBottom: '1.5rem',
    },
  },
  '.pl-xs': {
    paddingLeft: '1.25rem',

    '@screen md': {
      paddingLeft: '1.5rem',
    },
  },
  '.py-xs': {
    paddingBottom: '1.25rem',
    paddingTop: '1.25rem',

    '@screen md': {
      paddingTop: '1.5rem',
      paddingBottom: '1.5rem',
    },
  },
  '.px-xs': {
    paddingLeft: '1.25rem',
    paddingRight: '1.25rem',

    '@screen md': {
      paddingLeft: '1.5rem',
      paddingRight: '1.5rem',
    },
  },
  // sm
  '.p-sm': {
    padding: '2rem',

    '@screen md': {
      padding: '2.5rem',
    },
  },
  '.pt-sm': {
    paddingTop: '2rem',

    '@screen md': {
      paddingTop: '2.5rem',
    },
  },
  '.pr-sm': {
    paddingRight: '2rem',

    '@screen md': {
      paddingRight: '2.5rem',
    },
  },
  '.pb-sm': {
    paddingBottom: '2rem',

    '@screen md': {
      paddingBottom: '2.5rem',
    },
  },
  '.pl-sm': {
    paddingLeft: '2rem',

    '@screen md': {
      paddingLeft: '2.5rem',
    },
  },
  '.py-sm': {
    paddingBottom: '2rem',
    paddingTop: '2rem',

    '@screen md': {
      paddingTop: '2.5rem',
      paddingBottom: '2.5rem',
    },
  },
  '.px-sm': {
    paddingLeft: '2rem',
    paddingRight: '2rem',

    '@screen md': {
      paddingLeft: '2.5rem',
      paddingRight: '2.5rem',
    },
  },
  // md
  '.p-md': {
    padding: '2.5rem',

    '@screen md': {
      padding: '4rem',
    },
  },
  '.pt-md': {
    paddingTop: '2.5rem',

    '@screen md': {
      paddingTop: '4rem',
    },
  },
  '.pr-md': {
    paddingRight: '2.5rem',

    '@screen md': {
      paddingRight: '4rem',
    },
  },
  '.pb-md': {
    paddingBottom: '2.5rem',

    '@screen md': {
      paddingBottom: '4rem',
    },
  },
  '.pl-md': {
    paddingLeft: '2.5rem',

    '@screen md': {
      paddingLeft: '4rem',
    },
  },
  '.py-md': {
    paddingBottom: '2.5rem',
    paddingTop: '2.5rem',

    '@screen md': {
      paddingTop: '4rem',
      paddingBottom: '4rem',
    },
  },
  '.px-md': {
    paddingLeft: '2.5rem',
    paddingRight: '2.5rem',

    '@screen md': {
      paddingLeft: '4rem',
      paddingRight: '4rem',
    },
  },
  // lg
  '.p-lg': {
    padding: '4rem',

    '@screen md': {
      padding: '5rem',
    },
  },
  '.pt-lg': {
    paddingTop: '4rem',

    '@screen md': {
      paddingTop: '5rem',
    },
  },
  '.pr-lg': {
    paddingRight: '4rem',

    '@screen md': {
      paddingRight: '5rem',
    },
  },
  '.pb-lg': {
    paddingBottom: '4rem',

    '@screen md': {
      paddingBottom: '5rem',
    },
  },
  '.pl-lg': {
    paddingLeft: '4rem',

    '@screen md': {
      paddingLeft: '5rem',
    },
  },
  '.py-lg': {
    paddingBottom: '4rem',
    paddingTop: '4rem',

    '@screen md': {
      paddingTop: '5rem',
      paddingBottom: '5rem',
    },
  },
  '.px-lg': {
    paddingLeft: '4rem',
    paddingRight: '4rem',

    '@screen md': {
      paddingLeft: '5rem',
      paddingRight: '5rem',
    },
  },
  // lg
  '.p-lg': {
    padding: '4rem',

    '@screen md': {
      padding: '5rem',
    },
  },
  '.pt-lg': {
    paddingTop: '4rem',

    '@screen md': {
      paddingTop: '5rem',
    },
  },
  '.pr-lg': {
    paddingRight: '4rem',

    '@screen md': {
      paddingRight: '5rem',
    },
  },
  '.pb-lg': {
    paddingBottom: '4rem',

    '@screen md': {
      paddingBottom: '5rem',
    },
  },
  '.pl-lg': {
    paddingLeft: '4rem',

    '@screen md': {
      paddingLeft: '5rem',
    },
  },
  '.py-lg': {
    paddingBottom: '4rem',
    paddingTop: '4rem',

    '@screen md': {
      paddingTop: '5rem',
      paddingBottom: '5rem',
    },
  },
  '.px-lg': {
    paddingLeft: '4rem',
    paddingRight: '4rem',

    '@screen md': {
      paddingLeft: '5rem',
      paddingRight: '5rem',
    },
  },
  // xl
  '.p-xl': {
    padding: '5rem',

    '@screen md': {
      padding: '7.5rem',
    },
  },
  '.pt-xl': {
    paddingTop: '5rem',

    '@screen md': {
      paddingTop: '7.5rem',
    },
  },
  '.pr-xl': {
    paddingRight: '5rem',

    '@screen md': {
      paddingRight: '7.5rem',
    },
  },
  '.pb-xl': {
    paddingBottom: '5rem',

    '@screen md': {
      paddingBottom: '7.5rem',
    },
  },
  '.pl-xl': {
    paddingLeft: '5rem',

    '@screen md': {
      paddingLeft: '7.5rem',
    },
  },
  '.py-xl': {
    paddingBottom: '5rem',
    paddingTop: '5rem',

    '@screen md': {
      paddingTop: '7.5rem',
      paddingBottom: '7.5rem',
    },
  },
  '.px-xl': {
    paddingLeft: '5rem',
    paddingRight: '5rem',

    '@screen md': {
      paddingLeft: '7.5rem',
      paddingRight: '7.5rem',
    },
  },
  // xxl
  '.p-xxl': {
    padding: '7.5rem',

    '@screen md': {
      padding: '10rem',
    },
  },
  '.pt-xxl': {
    paddingTop: '7.5rem',

    '@screen md': {
      paddingTop: '10rem',
    },
  },
  '.pr-xxl': {
    paddingRight: '7.5rem',

    '@screen md': {
      paddingRight: '10rem',
    },
  },
  '.pb-xxl': {
    paddingBottom: '7.5rem',

    '@screen md': {
      paddingBottom: '10rem',
    },
  },
  '.pl-xxl': {
    paddingLeft: '7.5rem',

    '@screen md': {
      paddingLeft: '10rem',
    },
  },
  '.py-xxl': {
    paddingBottom: '7.5rem',
    paddingTop: '7.5rem',

    '@screen md': {
      paddingTop: '10rem',
      paddingBottom: '10rem',
    },
  },
  '.px-xxl': {
    paddingLeft: '7.5rem',
    paddingRight: '7.5rem',

    '@screen md': {
      paddingLeft: '10rem',
      paddingRight: '10rem',
    },
  },
// --------------------------------------------- Gap --------------------------------------------->>
  // gap-y
  '.gap-y-xs': {
    rowGap: '1.25rem',

    '@screen md': {
      rowGap: '1.5rem',
    },
  },
  '.gap-y-sm': {
    rowGap: '2rem',

    '@screen md': {
      rowGap: '2.5rem',
    },
  },
  '.gap-y-md': {
    rowGap: '2.5rem',

    '@screen md': {
      rowGap: '4rem',
    },
  },
  // gap-x
  '.gap-x-grid': {
    columnGap: '1.5rem',

    '@screen md': {
      columnGap: '2.5rem',
    },
  },
  '.gap-x-xs': {
    columnGap: '1.25rem',

    '@screen md': {
      columnGap: '1.5rem',
    },
  },
  '.gap-x-sm': {
    columnGap: '2rem',

    '@screen md': {
      columnGap: '2.5rem',
    },
  },
  // gap
  '.gap-xs': {
    columnGap: '1.25rem',
    rowGap: '1.25rem',

    '@screen md': {
      columnGap: '1.5rem',
      rowGap: '1.5rem',
    },
  },
  '.gap-sm': {
    columnGap: '2rem',
    rowGap: '2rem',

    '@screen md': {
      columnGap: '2.5rem',
      rowGap: '2.5rem',
    },
  },
}
