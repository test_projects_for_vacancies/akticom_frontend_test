/** @type {import('tailwindcss').Config} */
const grids = require('./config/tailwind/grids')
const spacings = require('./config/tailwind/spacings')
const typography = require('./config/tailwind/typography')
const common = require('./config/tailwind/common')

export default {
  important: true,
  content: [
    './src/style.scss',
    './src/styles/*/*.scss',
    './src/components/**/*.vue',
    './src/layouts/**/*.vue',
  ],
  theme: {
    colors: {
      'transparent': 'transparent',
      'black'      : '#171B21',
      'dark-blue'  : '#1F2B39',
      'dirty-blue' : '#1C274C',
      'gray'       : '#9BA8BB',
      'light-gray' : '#CFD5DC',
      'white'      : '#FFFFFF',
    },
    screens: {
      'min' : '320px',
      '2xs' : '375px',
      'xs'  : '480px',
      'sm'  : '768px',
      'md'  : '1024px',
      'lg'  : '1280px',
      'xl'  : '1441px',
      '2xl' : '1680px',
      'hd'  : '1920px',
    },
    extend: {
      fontSize: {
        'xl'   : ['1.125rem', '1.5rem' ],
        'lg'   : ['1rem',     '1.5rem' ],
        'md'   : ['0.875rem', '1.5rem' ],
        'sm'   : ['0.75rem',  '0.75rem'],
        'icon' : ['1.5rem',   '1.5rem' ],
        'h1-dl': ['1.5rem',   '2rem'   ],
      },
      zIndex: {
        '1': 1,
        '2': 2,
        '3': 3,
        '4': 4,
        '5': 5,
      },
      stroke: {
        'inherit': 'inherit',
      },
      fill: {
        'inherit': 'inherit',
      },
    },
  },
  plugins: [
    function ({ addBase, addComponents }) {
      addBase({
        '.container': {
          maxWidth: 'none',
          minWidth: '100%',
          marginLeft: '0',
          marginRight: '0',
          '--px': '1.25rem',
          '@screen md': {
            '--px': '5rem',
          },
        },
      })

      addComponents({
        ...common,
        ...grids,
        ...spacings,
        ...typography,
      })
    },
  ],
}

